.. _advanced_effective_harmonic_models:
.. highlight:: python
.. index::
   single: Advanced topics; Effective harmonic models

Effective harmonic models
=========================

Using :program:`hiPhive` it is straightforward to generate effective harmonic
models (EHMs) from molecular dynamics (MD) trajectories. These models enable
one to describe one for example the temperature dependence of the phonon
dispersion as well as derived quantities such as the harmonic free energy.

This example comprises two scripts: The first one carries out a series of MD
simulations using the EMT calculator from ASE, while the second scripts
generates EHMs for each temperature and analyzes the resulting phonon
dispersions.

The EHMs constructed here include interactions up to the second neighbor shell
as can be seen from the list of orbits::

  ===================================== List of Orbits =====================================
  index | order |      elements      |  radius  |     prototype      | clusters | parameters
  ------------------------------------------------------------------------------------------
    0   |   2   |       Ni Ni        |  0.0000  |       (0, 0)       |    1     |     1
    1   |   2   |       Ni Ni        |  1.2445  |       (0, 1)       |    6     |     3
    2   |   2   |       Ni Ni        |  2.4890  |       (0, 2)       |    6     |     3
    3   |   2   |       Ni Ni        |  2.1556  |       (0, 3)       |    12    |     4
    4   |   2   |       Ni Ni        |  3.0484  |      (0, 10)       |    4     |     2
    5   |   2   |       Ni Ni        |  2.7828  |      (0, 11)       |    12    |     4
    6   |   2   |       Ni Ni        |  1.7600  |      (0, 16)       |    3     |     2
  ==========================================================================================

With increasing temperature the ability of an EHM to map the forces present in
the structure decreases as evident from the root-mean-square errors over the
training and test sets:

    =============== ================== ==================
    Temperature (K) Train RMSE (meV/A) Test RMSE (meV/A)
    =============== ================== ==================
    200              74.6               75.3
    800             289.8              272.8
    1400            525.8              497.2
    =============== ================== ==================

The phonon dispersions obtained in this fashion are shown in the following
figure.

.. figure:: _static/phonon_dispersions_from_effective_harmonic_models.svg

  Phonon dispersions for FCC Ni from a series of effective harmonic models
  generated from MD trajectories recorded at different temperatures.

Source code
-----------

.. |br| raw:: html

   <br />

.. container:: toggle

    .. container:: header

       Rattled structures are generated in |br|
       ``tutorial/advanced/effective_harmonic_models/prepare_data.py``

    .. literalinclude:: ../../../tutorial/advanced/effective_harmonic_models/prepare_data.py

.. container:: toggle

    .. container:: header

       The force distributions are analyzed in |br|
       ``tutorial/advanced/effective_harmonic_models/generate_harmonic_models.py``

    .. literalinclude:: ../../../tutorial/advanced/effective_harmonic_models/generate_harmonic_models.py
