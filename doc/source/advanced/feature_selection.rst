.. _feature_selection:
.. highlight:: python
.. index::
   single: Advanced topics; Feature selection

Feature selection
=================

In this tutorial we explore the concept of feature selection while considering
three different methods

* `Lasso (least absolute shrinkage and selection operator) <http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html>`_
* `ARDR (automatic rRelevance detection regression) <http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.ARDRegression.html>`_
* `Bootstrapping (Bootstrap Aggregating) <https://en.wikipedia.org/wiki/Bootstrap_aggregating>`_

These methods solve the over or under-determined linear problem

.. math::
    \boldsymbol{A}\boldsymbol{x} = \boldsymbol{b}.

where :math:`\boldsymbol{A}` is the fit matrix, :math:`\boldsymbol{x}`
represents the parameters to be determined, and :math:`\boldsymbol{b}` are the
target values.

The present tutorial has the objective to showcase the different algorithms
and is not indented as a comprehensive introduction. The reader is rather
strongly encouraged to turn to the existing literature on feature selection in
machine learning. Here, the links include in the list above provide useful
starting points. It should also be noted that for instance `scikit-learn <http
://scikit-learn.org/>`_, which underpins the algorithms employed here,
provides further interesting feature selection techniques, such as the
recursive feature elimination, which can be used with :program:`hiPhive` with
little additional effort.


Lasso
-----

The `least absolute shrinkage and selection operator (LASSO) <http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html>`_
minimizes the objective function

.. math::

    \frac{1}{2 N} || \boldsymbol{A}\boldsymbol{x} - \boldsymbol{b} ||^2_2
    + \alpha ||\boldsymbol{x}||_1,

where :math:`\alpha` is the regularization parameter which determines the
sparsity of the solution and :math:`N` is the number of rows in
:math:`\boldsymbol{A}`.

Lasso thus aims to achieve sparse solutions by directly minimizing the
:math:`\ell_1`-norm of the model parameter vector

.. math::
    ||\boldsymbol{x}||_1 = n^{-1} \sum_i |x_i|,

where :math:`n` is the number of parameters (or elements of
:math:`\boldsymbol{x}`).

ARDR
----

`Automatic relevance detection regression (ARDR)
<http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.ARDRegression.html>`_
is a Bayesian regression techniques and thus seeks to find a solution, which
assigns priors to both the model parameters and the hyperparameters. It is also
known as sparse Bayesian learning (SBL) or relevance vector machine (RVM).

In ARDR the :math:`\lambda`-threshold parameter can be used for
removing (pruning) weights with high precision from the computation.


Boostrap
--------

`Bootstrapping (Bootstrap Aggregating) <https://en.wikipedia.org/wiki/Bootstrap_aggregating>`_ can be used for pruning models via removing parameters according
to the criterion

.. math::

    \forall x_i \in \boldsymbol{x}
    \quad
    x_i =
    \begin{cases}
        \left<x_i\right> & \text{if}\qquad \left|\left<x_i\right>\right| > f \sigma(x_i) \\
        0                & \text{else}
    \end{cases}

where mean :math:`\left<x_i\right>` and standard deviation
:math:`\sigma(x_i)` are defined over an ensemble of models.


Figures
-------

The feature selection process looks like

.. figure:: _static/feature_selection_methods.svg

The obtained force constant models for a few selection parameters are shown below.

.. figure:: _static/feature_selection_models.svg


Source code
-----------

.. |br| raw:: html

   <br />

.. container:: toggle

    .. container:: header

        Structure preparation |br|
        ``tutorial/advanced/feature_selection/prepare_structures.py``

    .. literalinclude:: ../../../tutorial/advanced/feature_selection/prepare_structures.py

.. container:: toggle

    .. container:: header

        Setup of structure container |br|
        ``tutorial/advanced/feature_selection/setup_structure_container.py``

    .. literalinclude:: ../../../tutorial/advanced/feature_selection/setup_structure_container.py

.. container:: toggle

    .. container:: header

        Feature selection |br|
        ``tutorial/advanced/feature_selection/feature_selection.py``

    .. literalinclude:: ../../../tutorial/advanced/feature_selection/feature_selection.py
