Cluster space
=============

.. module:: hiphive

.. index::
   single: Class reference; ClusterSpace

ClusterSpace
------------

.. autoclass:: ClusterSpace
   :members:
   :undoc-members:


.. index::
   single: Class reference; Cutoffs

Cutoffs
-------

.. autoclass:: hiphive.cluster_filter.Cutoffs
   :members:
   :undoc-members:
