.. index::
   single: Function reference; input/output
   single: Class reference; input/output

Input/output
============

The functions and modules described below provide functionality for controlling
the verbosity of the output as well as for reading and writing force constant
matrices in different formats.

Logging
-------
.. autofunction:: hiphive.io.logging.set_config

phonopy data files
------------------
.. automodule:: hiphive.io.phonopy
   :members:
   :undoc-members:

shengBTE data files
-------------------
.. automodule:: hiphive.io.shengBTE
   :members:
   :undoc-members:
