.. index::
   single: Class reference; Optimizers

.. module:: hiphive.fitting

Optimizers
==========


.. index::
   single: Class reference; Optimizer

Optimizer
---------

.. autoclass:: Optimizer
   :members:
   :undoc-members:
   :inherited-members:


.. index::
   single: Class reference; EnsembleOptimizer

EnsembleOptimizer
-----------------

.. autoclass:: EnsembleOptimizer
   :members:
   :undoc-members:
   :inherited-members:


.. index::
   single: Class reference; CrossValidationEstimator

CrossValidationEstimator
------------------------

.. autoclass:: CrossValidationEstimator
   :members:
   :undoc-members:
   :inherited-members:
