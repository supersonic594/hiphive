.. raw:: html

  <p>
  <a href="https://gitlab.com/materials-modeling/hiphive/commits/master"><img alt="pipeline status" src="https://gitlab.com/materials-modeling/hiphive/badges/master/pipeline.svg" /></a>
  <a href="https://hiphive.materialsmodeling.org/coverage"><img alt="coverage report" src="https://gitlab.com/materials-modeling/hiphive/badges/master/coverage.svg" /></a>
  <a href="https://badge.fury.io/py/hiphive"><img src="https://badge.fury.io/py/hiphive.svg" alt="PyPI version" height="18"></a>
  </p>
  

:program:`hiPhive` — High-order force constants for the masses
**************************************************************

:program:`hiPhive` is a tool for efficiently extracting high-order force
constants from atomistic simulations, most commonly density functional theory
calculations. It has been implemented in the form of a Python library, which
allows it to be readily integrated with many first-principles codes and
analysis tools accessible in Python.

The following snippet illustrates the construction of a force constant
potential object (``fcp``), which  can be subsequently used to run molecular
dynamics (MD) simulations or analyzed using e.g., `phonopy
<https://atztogo.github.io/phonopy/>`_, `phono3py
<https://atztogo.github.io/phono3py/>`_, or `shengBTE
<http://www.shengbte.org/>`_::

    from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
    from hiphive.fitting import Optimizer

    cs = ClusterSpace(ideal_cell, cutoffs=[6.0, 4.5])
    sc = StructureContainer(cs)
    for atoms in list_of_training_structures:
        sc.add_structure(atoms)
    opt = Optimizer(sc.get_fit_data())
    opt.train()
    fcp = ForceConstantPotential(cs, opt.parameters)

The list of training structures employed in the code above can be prepared for
example as follows::

    from ase.build import bulk
    from ase.calculators.emt import EMT

    ideal_cell = bulk('Al', cubic=True)
    ideal_supercell = ideal_cell.repeat(3)
    list_of_training_structures = []
    for k in range(5):
        atoms = ideal_supercell.copy()
        atoms.rattle(0.02, seed=k)
        atoms.set_calculator(EMT())
        atoms.new_array('displacements', atoms.positions - ideal_supercell.positions)
        atoms.new_array('forces', atoms.get_forces())
        atoms.positions = ideal_supercell.get_positions()
        list_of_training_structures.append(atoms)

:program:`hiPhive` and its development are hosted on `gitlab
<https://gitlab.com/materials-modeling/hiphive>`_. Bugs and feature
requests are ideally submitted via the `gitlab issue tracker
<https://gitlab.com/materials-modeling/hiphive/issues>`_. The development
team can also be reached by email via hiphive@materialsmodeling.org.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   overview
   installation
   background
   workflow
   basic/index
   advanced/index
   moduleref/index
   benchmarks

   bibliography
   glossary
   genindex
