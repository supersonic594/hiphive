import numpy as np

from ase.build import bulk
from hiphive.cluster_filter import estimate_maximum_cutoff, is_cutoff_allowed

tol = 1e-3  # precision of estimated maximum cutoff

# Simple cubic 3x3x3, theoretical max_cutoff is 2.0
atoms = bulk('Al', 'sc', a=1.0, cubic=True).repeat(3)
maximum_cutoff = 2.0

# Find maximum cutoff
estimated_maximum_cutoff = estimate_maximum_cutoff(atoms)
assert (estimated_maximum_cutoff - maximum_cutoff) < tol

# Make sure is_cutoff_allowed works correctly
allowed_cutoffs = np.linspace(0.0, maximum_cutoff - tol, 15)
nonallowed_cutoffs = np.linspace(maximum_cutoff + tol, 2*maximum_cutoff, 15)
assert all(is_cutoff_allowed(atoms, c) for c in allowed_cutoffs)
assert all(not is_cutoff_allowed(atoms, c) for c in nonallowed_cutoffs)
