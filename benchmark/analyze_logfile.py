#!/usr/bin/env python3
"""
Analyze a specific logfile
"""

import argparse
import numpy as np
from datetime import datetime
from collections import namedtuple, OrderedDict


Task = namedtuple('Task', ['name', 'time'])
date_format = '%Y-%m-%d %H:%M:%S.%f'


def parse_logfile(logfile):
    summary = OrderedDict()

    summary['ClusterSpace'] = _parse_cluster_space_log(logfile)
    summary['ForceConstantModel'] = _parse_force_constant_model(logfile)
    summary['Compute FitMatrix'] = _parse_compute_fit_matrix(logfile)
    return summary


def _parse_cluster_space_log(logfile):
    main_key = 'building ClusterSpace'
    task_keys = [
        'categorizing clusters', 'finding eigentensors',
        'constructing constraint matrices', 'constructing constraint vectors']

    main_task = Task(main_key, _parse_task_time(logfile, main_key))
    sub_tasks = []
    for key in task_keys:
        delta_time = _parse_task_time(logfile, key)
        sub_tasks.append(Task(key, delta_time))

    time_sum = sum([task.time for task in sub_tasks
                    if not np.isnan(task.time)])
    sub_tasks.append(Task('others', main_task.time - time_sum))
    return main_task, sub_tasks


def _parse_force_constant_model(logfile):
    main_key = 'building ForceConstantModel'
    main_task = Task(main_key, _parse_task_time(logfile, main_key))
    return main_task, []


def _parse_compute_fit_matrix(logfile):
    main_key = 'computing fit matrix'
    main_task = Task(main_key, _parse_task_time(logfile, main_key))
    return main_task, []


def _parse_task_time(logfile, task):
    start_line = 'Starting ' + task
    end_line = 'Finished ' + task

    with open(logfile, 'r') as f:
        lines = f.readlines()
        time_lines = [' '.join(line.split()[:2]) for line in lines]
        info_lines = [' '.join(line.split()[2:]) for line in lines]
        for i, (date, info) in enumerate(zip(time_lines, info_lines)):
            if info == start_line:
                start_time = datetime.strptime(date, date_format)
                for date2, info2 in zip(time_lines[i:], info_lines[i:]):
                    if info2 == end_line:
                        end_time = datetime.strptime(date2, date_format)
                        return (end_time - start_time).total_seconds()
    return np.nan


def print_logfile_summary(summary):
    width = 62
    for name, (main_task, sub_tasks) in summary.items():
        print(name.center(width, '-'))
        print('{:38} : {:10.3f} sec'.format(main_task.name, main_task.time))
        for task in sub_tasks:
            print('    {:34} : {:10.3f} sec'.format(task.name, task.time))
        print('')


parser = argparse.ArgumentParser(
    description='Analyze a timestamped logfile from hiphive and print timing'
    'for different functions')
parser.add_argument('logfile', help='hiphive timestamped logfile')

args = parser.parse_args()

summary = parse_logfile(args.logfile)
print_logfile_summary(summary)
